<?php

namespace App\Http\Controllers;

use App\Models\BTS;
use Illuminate\Http\Request;

class BTSController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $bts = BTS::all();
        return view('bts.index', compact('bts'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('bts.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'alamat' => ['required', 'string'],
        ],
            [
                'name.required' => 'Kolom nama BTS wajib diisi.',
                'name.string' => 'Kolom nama BTS harus menggunakan string.',
                'alamat.required' => 'Kolom alamat BTS wajib diisi.',
                'alamat.string' => 'Kolom alamat BTS harus menggunakan string.',
            ]
        );
        $bts = new BTS;
        $bts->name = $request->name;
        $bts->alamat = $request->alamat;
        $bts->save();
        return redirect()->route('bts.index')->with('success', 'BTS created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $bts = BTS::where('id', $id)->first();
        return view('bts.edit', compact('bts'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'alamat' => ['required', 'string'],
        ],
            [
                'name.required' => 'Kolom nama BTS wajib diisi.',
                'name.string' => 'Kolom nama BTS harus menggunakan string.',
                'alamat.required' => 'Kolom alamat BTS wajib diisi.',
                'alamat.string' => 'Kolom alamat BTS harus menggunakan string.',
            ]
        );
        $bts = BTS::where('id', $id)->first();
        $bts->name = $request->name;
        $bts->alamat = $request->alamat;
        $bts->save();
        return redirect()->route('bts.index')->with('success', 'BTS updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
