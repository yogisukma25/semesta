<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\JenisIdentitas;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($role)
    {
        $karyawan = User::hasRoles($role)->get();
        return view('karyawan.index', compact('karyawan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $role = Role::all();
        $jenisidentitas = JenisIdentitas::all();
        return view('karyawan.create', compact('jenisidentitas'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:' . User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'id_jenis_identitas' => ['required'],
            'nomor_identitas' => ['required', 'string', 'max:255'],
            'tempat_lahir' => ['required'],
            'tanggal_lahir' => ['required'],
            'alamat' => ['required'],
            'nomor_telepon' => ['required', 'string', 'max:255'],
            'catatan' => ['nullable'],
            'status' => ['required'],
            'role' => ['required'],
        ],
            [
                'name.required' => 'Kolom nama wajib diisi.',
                'name.string' => 'Kolom nama harus menggunakan string.',
                'email.required' => 'Email harus diisi.',
                'email.string' => 'Email harus menggunaknan string.',
                'email.lowercase' => 'Email harus huruf kecil.',
                'email.email' => 'Email harus dengan format @gmail.com.',
                'email.unique' => 'Email harus unik.',
                'id_jenis_identitas.required' => 'Jenis identitas harus diisi.',
                'nomor_identitas.required' => 'Nomor identitas harus diisi.',
                'tempat_lahir.required' => 'Tempat lahir harus diisi.',
                'tanggal_lahir.required' => 'Tanggal lahir harus diisi.',
                'alamat.required' => 'Kolom alamat wajib diisi.',
                'nomor_telepon.required' => 'Nomor telepon harus diisi.',
                'status.required' => 'Status harus diisi.',
                'role.required' => 'Status harus diisi.',
            ]
        );
        $karyawan = new User;
        $karyawan->name = $request->name;
        $karyawan->email = $request->email;
        $karyawan->password = Hash::make($request->password);
        $karyawan->id_jenis_identitas = $request->id_jenis_identitas;
        $karyawan->nomor_identitas = $request->nomor_identitas;
        $karyawan->tempat_lahir = $request->tempat_lahir;
        $karyawan->tanggal_lahir = $request->tanggal_lahir;
        $karyawan->alamat = $request->alamat;
        $karyawan->nomor_telepon = $request->nomor_telepon;
        $karyawan->catatan = $request->catatan;
        $karyawan->status = $request->status;
        $karyawan->save();
        $karyawan->assignRole($request->role);
        return redirect()->route('teknisi.index')->with('success', 'Karyawan created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $teknisi = User::where('id', $id)->first();
        $role = Role::all();

        $jenisidentitas = JenisIdentitas::all();
        return view('teknisi.edit', compact('teknisi', 'jenisidentitas'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:' . User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'id_jenis_identitas' => ['required'],
            'nomor_identitas' => ['required', 'string', 'max:255'],
            'tempat_lahir' => ['required'],
            'tanggal_lahir' => ['required'],
            'alamat' => ['required'],
            'nomor_telepon' => ['required', 'string', 'max:255'],
            'catatan' => ['nullable'],
            'status' => ['required'],
        ],
            [
                'name.required' => 'Kolom nama wajib diisi.',
                'name.string' => 'Kolom nama harus menggunakan string.',
                'email.required' => 'Email harus diisi.',
                'email.string' => 'Email harus menggunaknan string.',
                'email.lowercase' => 'Email harus huruf kecil.',
                'email.email' => 'Email harus dengan format @gmail.com.',
                'email.unique' => 'Email harus unik.',
                'id_jenis_identitas.required' => 'Jenis identitas harus diisi.',
                'nomor_identitas.required' => 'Nomor identitas harus diisi.',
                'tempat_lahir.required' => 'Tempat lahir harus diisi.',
                'tanggal_lahir.required' => 'Tanggal lahir harus diisi.',
                'alamat.required' => 'Kolom alamat wajib diisi.',
                'nomor_telepon.required' => 'Nomor telepon harus diisi.',
                'status.required' => 'Status harus diisi.',
            ]
        );
        $karyawan = User::where('id', $id)->first();
        $karyawan->name = $request->name;
        $karyawan->email = $request->email;
        $karyawan->password = Hash::make($request->password);
        $karyawan->id_jenis_identitas = $request->id_jenis_identitas;
        $karyawan->nomor_identitas = $request->nomor_identitas;
        $karyawan->tempat_lahir = $request->tempat_lahir;
        $karyawan->tanggal_lahir = $request->tanggal_lahir;
        $karyawan->alamat = $request->alamat;
        $karyawan->nomor_telepon = $request->nomor_telepon;
        $karyawan->catatan = $request->catatan;
        $karyawan->status = $request->status;
        $karyawan->save();
        $role = $karyawan->getRoleNames();
        $karyawan->removeRole($role);
        $karyawan->assignRole($request->role);
        return redirect()->route('karyawan.index')->with('success', 'Karyawan updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
