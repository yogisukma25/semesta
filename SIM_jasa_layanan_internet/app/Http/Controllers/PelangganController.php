<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BTS;
use App\Models\JenisIdentitas;
use App\Models\Loket;
use App\Models\PaketBerlangganan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pelanggan = User::hasRoles('pelanggan')->get();
        return view('pelanggan.index', compact('pelanggan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $jenisidentitas = JenisIdentitas::all();
        $bts = BTS::all();
        $loket = Loket::all();
        $paket = PaketBerlangganan::all();
        return view('pelanggan.create', compact('jenisidentitas', 'bts', 'loket', 'paket'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:' . User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'tanggal_pemasangan' => ['required'],
            'id_jenis_identitas' => ['required'],
            'nomor_identitas' => ['required', 'string', 'max:255'],
            'tempat_lahir' => ['required'],
            'tanggal_lahir' => ['required'],
            'alamat' => ['required'],
            'latitude' => ['nullable'],
            'longitude' => ['nullable'],
            'nomor_telepon' => ['required', 'string', 'max:255'],
            'id_bts' => ['required'],
            'id_paket_berlangganan' => ['required'],
            'id_loket' => ['required'],
            'catatan' => ['nullable'],
            'status' => ['required'],
        ],
            [
                'name.required' => 'Kolom nama wajib diisi.',
                'name.string' => 'Kolom nama harus menggunakan string.',
                'email.required' => 'Email harus diisi.',
                'email.string' => 'Email harus menggunaknan string.',
                'email.lowercase' => 'Email harus huruf kecil.',
                'email.email' => 'Email harus dengan format @gmail.com.',
                'email.unique' => 'Email harus unik.',
                'tanggal_pemasangan.required' => 'Tanggal harus diisi.',
                'id_jenis_identitas.required' => 'Jenis identitas harus diisi.',
                'nomor_identitas.required' => 'Nomor identitas harus diisi.',
                'tempat_lahir.required' => 'Tempat lahir harus diisi.',
                'tanggal_lahir.required' => 'Tanggal lahir harus diisi.',
                'alamat.required' => 'Kolom alamat wajib diisi.',
                'nomor_telepon.required' => 'Nomor telepon harus diisi.',
                'id_bts.required' => 'BTS harus diisi.',
                'id_paket_berlangganan.required' => 'Paket harus diisi.',
                'id_loket.required' => 'Loket harus diisi.',
                'status.required' => 'Status harus diisi.',
            ]
        );
        $pelanggan = new User;
        $pelanggan->name = $request->name;
        $pelanggan->email = $request->email;
        $pelanggan->password = Hash::make($request->password);
        $pelanggan->tanggal_pemasangan = $request->tanggal_pemasangan;
        $pelanggan->id_jenis_identitas = $request->id_jenis_identitas;
        $pelanggan->nomor_identitas = $request->nomor_identitas;
        $pelanggan->tempat_lahir = $request->tempat_lahir;
        $pelanggan->tanggal_lahir = $request->tanggal_lahir;
        $pelanggan->alamat = $request->alamat;
        $pelanggan->latitude = $request->latitude;
        $pelanggan->longitude = $request->longitude;
        $pelanggan->nomor_telepon = $request->nomor_telepon;
        $pelanggan->id_bts = $request->id_bts;
        $pelanggan->id_paket_berlangganan = $request->id_paket_berlangganan;
        $pelanggan->id_loket = $request->id_loket;
        $pelanggan->catatan = $request->catatan;
        $pelanggan->status = $request->status;
        $pelanggan->save();
        $pelanggan->id_pelanggan = $request->id_bts + $pelanggan->id;
        $pelanggan->save();
        return redirect()->route('pelanggan.index')->with('success', 'Pelanggan created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $pelanggan = User::where('id', $id)->first();
        $jenisidentitas = JenisIdentitas::all();
        $bts = BTS::all();
        $loket = Loket::all();
        $paket = PaketBerlangganan::all();
        return view('pelanggan.edit', compact('pelanggan', 'jenisidentitas', 'bts', 'loket', 'paket'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:' . User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'tanggal_pemasangan' => ['required'],
            'id_jenis_identitas' => ['required'],
            'nomor_identitas' => ['required', 'string', 'max:255'],
            'tempat_lahir' => ['required'],
            'tanggal_lahir' => ['required'],
            'alamat' => ['required'],
            'latitude' => ['nullable'],
            'longitude' => ['nullable'],
            'nomor_telepon' => ['required', 'string', 'max:255'],
            'id_bts' => ['required'],
            'id_paket_berlangganan' => ['required'],
            'id_loket' => ['required'],
            'catatan' => ['nullable'],
            'status' => ['required'],
        ],
            [
                'name.required' => 'Kolom nama wajib diisi.',
                'name.string' => 'Kolom nama harus menggunakan string.',
                'email.required' => 'Email harus diisi.',
                'email.string' => 'Email harus menggunaknan string.',
                'email.lowercase' => 'Email harus huruf kecil.',
                'email.email' => 'Email harus dengan format @gmail.com.',
                'email.unique' => 'Email harus unik.',
                'tanggal_pemasangan.required' => 'Tanggal harus diisi.',
                'id_jenis_identitas.required' => 'Jenis identitas harus diisi.',
                'nomor_identitas.required' => 'Nomor identitas harus diisi.',
                'tempat_lahir.required' => 'Tempat lahir harus diisi.',
                'tanggal_lahir.required' => 'Tanggal lahir harus diisi.',
                'alamat.required' => 'Kolom alamat wajib diisi.',
                'nomor_telepon.required' => 'Nomor telepon harus diisi.',
                'id_bts.required' => 'BTS harus diisi.',
                'id_paket_berlangganan.required' => 'Paket harus diisi.',
                'id_loket.required' => 'Loket harus diisi.',
                'status.required' => 'Status harus diisi.',
            ]
        );
        $pelanggan = User::where('id', $id)->first();
        $pelanggan->id_pelanggan = $request->id_bts + $id;
        $pelanggan->name = $request->name;
        $pelanggan->email = $request->email;
        $pelanggan->password = Hash::make($request->password);
        $pelanggan->tanggal_pemasangan = $request->tanggal_pemasangan;
        $pelanggan->id_jenis_identitas = $request->id_jenis_identitas;
        $pelanggan->nomor_identitas = $request->nomor_identitas;
        $pelanggan->tempat_lahir = $request->tempat_lahir;
        $pelanggan->tanggal_lahir = $request->tanggal_lahir;
        $pelanggan->alamat = $request->alamat;
        $pelanggan->latitude = $request->latitude;
        $pelanggan->longitude = $request->longitude;
        $pelanggan->nomor_telepon = $request->nomor_telepon;
        $pelanggan->id_bts = $request->id_bts;
        $pelanggan->id_paket_berlangganan = $request->id_paket_berlangganan;
        $pelanggan->id_loket = $request->id_loket;
        $pelanggan->catatan = $request->catatan;
        $pelanggan->status = $request->status;
        $pelanggan->save();
        return redirect()->route('pelanggan.index')->with('success', 'Pelanggan updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
