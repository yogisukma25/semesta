<?php

namespace App\Http\Controllers;

use App\Models\BulanTagihan;
use Illuminate\Http\Request;

class BulanTagihanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $bulantagihan = BulanTagihan::all();
        return view('bulantagihan.index', compact('bulantagihan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('bulantagihan.create');

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'bulan' => ['required'],
            'tahun' => ['required'],
        ],
            [
                'bulan.required' => 'Kolom bulan tagihan wajib diisi.',
                'tahun.required' => 'Kolom tahun tagihan wajib diisi.',
            ]
        );
        $bulantagihan = new BulanTagihan;
        $bulantagihan->bulan = $request->bulan;
        $bulantagihan->tahun = $request->tahun;
        $bulantagihan->save();
        $pelanggan_all = User::hasRole('pelanggan')->get();
        foreach ($pelanggan_all as $pelanggan) {
            $tagihan = new PembayaranTagihan;
            $tagihan->id_bulan = $bulantagihan->id;
            $tagihan->id_pelanggan = $pelanggan->id_pelanggan;
            $tagihan->id_loket = $pelanggan->id_loket;
            $tagihan->save();
        }
        return redirect()->route('bulantagihan.index')->with('success', 'Bulan tagihan created successfully');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $bulantagihan = BulanTagihan::where('id', $id)->first();
        return view('bulantagihan.edit', compact('bulantagihan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'bulan' => ['required'],
            'tahun' => ['required'],
        ],
            [
                'bulan.required' => 'Kolom bulan tagihan wajib diisi.',
                'tahun.required' => 'Kolom tahun tagihan wajib diisi.',
            ]
        );
        $bulantagihan = BulanTagihan::where('id', $id)->first();
        $bulantagihan->bulan = $request->bulan;
        $bulantagihan->tahun = $request->tahun;
        $bulantagihan->save();
        return redirect()->route('bulantagihan.index')->with('success', 'Bulan Tagihan updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
