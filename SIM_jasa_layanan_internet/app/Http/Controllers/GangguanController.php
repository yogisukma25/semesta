<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Gangguan;
use App\Models\PivotGangguan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GangguanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $gangguan = Gangguan::all();
        return view('gangguan.index', compact('gangguan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $pelanggan = User::hasRoles('pelanggan')->get();
        return view('gangguan.create', compact('pelanggan'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_pelanggan' => ['required'],
            'deskripsi' => ['required', 'string'],
        ],
            [
                'id_pelanggan.required' => 'Kolom pelanggan gangguan wajib diisi.',
                'deskripsi.required' => 'Kolom deskripsi gangguan wajib diisi.',
                'deskripsi.string' => 'Kolom deskripsi gangguan harus menggunakan string.',
            ]
        );
        $gangguan = new Gangguan;
        $gangguan->id_pelanggan = $request->id_pelanggan;
        $gangguan->deskripsi = $request->deskripsi;
        $gangguan->save();
        return redirect()->route('gangguan.index')->with('success', 'Gangguan created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function ambil(string $id)
    {
        $gangguan = Gangguan::where('id', $id)->first();
        $gangguan->id_teknisi = Auth::user()->id;
        $gangguan->status = 'ditangani';
        $gangguan->save();
        return redirect()->route('gangguan.show')->with('success', 'Tugas berhasil diambil.');
    }
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $gangguan = Gangguan::where('id', $id)->first();
        return view('gangguan.show', compact('gangguan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function upload(Request $request, string $id)
    {
        $request->validate([
            'bukti' => ['required', 'image', 'mimes:jpeg,png,jpg', 'max:2048'],
            'catatan' => ['nullable', 'string'],
        ],
            [
                'bukti.required' => 'Kolom bukti perbaikan wajib diisi.',
                'catatan.string' => 'Kolom catatan perbaikan harus menggunakan string.',
            ]
        );
        $gangguan = Gangguan::where('id', $id)->first();
        $gangguan->bukti = $request->file('bukti')->store('bukti_perbaikan');
        $gangguan->catatan = $request->catatan;
        if ($request->teknisi_bantu != null) {
            foreach ($request->teknisi_bantu as $teknisi) {
                $teknisi_bantu = new PivotGangguan;
                $teknisi_bantu->id_gangguan = $gangguan->id;
                $teknisi_bantu->id_teknisi = $teknisi;
                $user = User::where('id', $teknisi)->first();
                $user->poin = $user->poin + 1;
                $user->save();
                $teknisi_bantu->save();
            }
        }
        $user = User::where('id', $gangguan->id_teknisi)->first();
        $user->poin = $user->poin + 1;
        $user->save();
        $gangguan->status = 'selesai';
        $gangguan->save();
        return redirect()->route('gangguan.show')->with('success', 'Tugas selesai.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
