<?php

namespace App\Http\Controllers;

use App\Models\JenisIdentitas;
use Illuminate\Http\Request;

class JenisIdentitasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $jenisidentitas = JenisIdentitas::all();
        return view('jenisidentitas.index', compact('jenisidentitas'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('jenisidentitas.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
        ],
            [
                'name.required' => 'Kolom nama jenis identitas wajib diisi.',
                'name.string' => 'Kolom nama jenis identitas harus menggunakan string.',
            ]
        );
        $jenisidentitas = new JenisIdentitas;
        $jenisidentitas->name = $request->name;
        $jenisidentitas->save();
        return redirect()->route('jenisidentitas.index')->with('success', 'Jenis Identitas created successfully');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $jenisidentitas = JenisIdentitas::where('id', $id)->first();
        return view('jenisidentitas.edit', compact('jenisidentitas'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
        ],
            [
                'name.required' => 'Kolom nama jenis identitas wajib diisi.',
                'name.string' => 'Kolom nama jenis identitas harus menggunakan string.',
            ]
        );
        $jenisidentitas = JenisIdentitas::where('id', $id)->first();
        $jenisidentitas->name = $request->name;
        $jenisidentitas->save();
        return redirect()->route('jenisidentitas.index')->with('success', 'Jenis Identitas updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
