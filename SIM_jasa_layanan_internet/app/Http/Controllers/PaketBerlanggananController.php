<?php

namespace App\Http\Controllers;

use App\Models\PaketBerlangganan;
use Illuminate\Http\Request;

class PaketBerlanggananController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $paket = PaketBerlangganan::all();
        return view('paket.index', compact('paket'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('paket.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'kecepatan' => ['required', 'string'],
            'harga' => ['required'],
        ],
            [
                'name.required' => 'Kolom nama paket wajib diisi.',
                'name.string' => 'Kolom nama paket harus menggunakan string.',
                'kecepatan.required' => 'Kolom kecepatan paket wajib diisi.',
                'kecepatan.string' => 'Kolom kecepatan paket harus menggunakan string.',
                'harga.required' => 'Kolom harga paket wajib diisi.',
            ]
        );
        $paket = new PaketBerlangganan;
        $paket->name = $request->name;
        $paket->kecepatan = $request->kecepatan;
        $paket->harga = $request->harga;
        $paket->save();
        return redirect()->route('paket.index')->with('success', 'Paket Berlangganan created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $paket = PaketBerlangganan::where('id', $id)->first();
        return view('paket.edit', compact('paket'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'kecepatan' => ['required', 'string'],
            'harga' => ['required'],
        ],
            [
                'name.required' => 'Kolom nama paket wajib diisi.',
                'name.string' => 'Kolom nama paket harus menggunakan string.',
                'kecepatan.required' => 'Kolom kecepatan paket wajib diisi.',
                'kecepatan.string' => 'Kolom kecepatan paket harus menggunakan string.',
                'harga.required' => 'Kolom harga paket wajib diisi.',
            ]
        );
        $paket = PaketBerlangganan::where('id', $id)->first();
        $paket->name = $request->name;
        $paket->kecepatan = $request->kecepatan;
        $paket->harga = $request->harga;
        $paket->save();
        return redirect()->route('paket.index')->with('success', 'Paket Berlangganan updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
