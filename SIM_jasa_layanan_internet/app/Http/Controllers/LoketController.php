<?php

namespace App\Http\Controllers;

use App\Models\Loket;
use Illuminate\Http\Request;

class LoketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $loket = Loket::all();
        return view('loket.index', compact('loket'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('loket.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'alamat' => ['required', 'string'],
        ],
            [
                'name.required' => 'Kolom nama loket wajib diisi.',
                'name.string' => 'Kolom nama loket harus menggunakan string.',
                'alamat.required' => 'Kolom alamat loket wajib diisi.',
                'alamat.string' => 'Kolom alamat loket harus menggunakan string.',
            ]
        );
        $loket = new Loket;
        $loket->name = $request->name;
        $loket->alamat = $request->alamat;
        $loket->save();
        return redirect()->route('loket.index')->with('success', 'Loket created successfully');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $loket = Loket::where('id', $id)->first();
        return view('loket.edit', compact('loket'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'alamat' => ['required', 'string'],
        ],
            [
                'name.required' => 'Kolom nama loket wajib diisi.',
                'name.string' => 'Kolom nama loket harus menggunakan string.',
                'alamat.required' => 'Kolom alamat loket wajib diisi.',
                'alamat.string' => 'Kolom alamat loket harus menggunakan string.',
            ]
        );
        $loket = Loket::where('id', $id)->first();
        $loket->name = $request->name;
        $loket->alamat = $request->alamat;
        $loket->save();
        return redirect()->route('loket.index')->with('success', 'loket updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
