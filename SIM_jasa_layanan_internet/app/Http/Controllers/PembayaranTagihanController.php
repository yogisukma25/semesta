<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BulanTagihan;
use App\Models\PembayaranTagihan;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PembayaranTagihanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $bulan = BulanTagihan::all();
        return view('tagihan.index', compact('bulan'));
    }
    /**
     * Display a listing of the resource.
     */
    public function tagihan($id_bulan)
    {
        if (Auth::user()->hasRole('admin')) {
            $tagihan = PembayaranTagihan::where('id_bulan', $id_bulan)->get();
            return view('tagihan.all', compact('tagihan', 'bulan'));
        }
        if (Auth::user()->hasRole('loket')) {
            $tagihan = PembayaranTagihan::where('id_loket', Auth::user->id_loket)->where('id_bulan', $id_bulan)->get();
            $bulan = $id_bulan;
            return view('tagihan.all', compact('tagihan', 'bulan'));
        }
    }
    /**
     * Display a listing of the resource.
     */
    public function tagihan_loket($id_loket, $id_bulan)
    {
        if (Auth::user()->hasRole('admin')) {
            $tagihan = PembayaranTagihan::where('id_loket', $id_loket)->where('id_bulan', $id_bulan)->get();
            $bulan = $id_bulan;
            return view('tagihan.loket', compact('tagihan', 'bulan'));
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $tagihan = PembayaranTagihan::where('id', $id)->first();
        return view('tagihan.edit', compact('tagihan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'jumlah_pembayaran' => ['required'],
            'tanggal_bayar' => ['required'],
            'status_bayar' => ['required'],
            'catatan' => ['required'],
        ],
            [
                'jumlah_pembayaran.required' => 'Kolom jumlah bayar tagihan wajib diisi.',
                'tanggal_bayar.required' => 'Kolom tanggal bayar tagihan wajib diisi.',
                'status_bayar.required' => 'Kolom status tagihan wajib diisi.',
                'catatan.required' => 'Kolom catatan tagihan wajib diisi.',
            ]
        );
        $tagihan = PembayaranTagihan::where('id', $id)->first();
        $tagihan->jumlah_pembayaran = $request->jumlah_pembayaran;
        $tagihan->tanggal_bayar = $request->tanggal_bayar;
        $tagihan->status_bayar = $request->status_bayar;
        $tagihan->catatan = $request->catatan;
        $tagihan->save();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
