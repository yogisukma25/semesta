<?php

namespace App\Models;

use App\Models\User;
use App\Models\Loket;
use App\Models\BulanTagihan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PembayaranTagihan extends Model
{
    use HasFactory;
    protected $table = 'pembayaran_tagihan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_bulan_tagihan',
        'id_pelanggan',
        'id_loket',
        'jumlah_bayar',
        'tanggal_bayar',
        'catatan',
        'status',
    ];

    public function users(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function bulanTagihan(): BelongsTo
    {
        return $this->belongsTo(BulanTagihan::class);
    }
    public function loket(): BelongsTo
    {
        return $this->belongsTo(Loket::class);
    }

}
