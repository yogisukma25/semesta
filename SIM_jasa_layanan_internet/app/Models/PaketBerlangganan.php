<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PaketBerlangganan extends Model
{
    use HasFactory;
    protected $table = 'paket_berlangganan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'kecepatan',
        'harga',
    ];

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }
}
