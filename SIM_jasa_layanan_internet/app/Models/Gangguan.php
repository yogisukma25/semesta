<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gangguan extends Model
{
    use HasFactory;
    protected $table = 'gangguan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_pelanggan',
        'id_teknisi',
        'deskripsi',
        'bukti',
        'catatan',
        'status',
    ];

    public function users(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function teknisi(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function teknisiBantu(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'pivot_gangguan', 'id_teknisi', 'id_gangguan');
    }
}
