<?php

namespace App\Models;

use App\Models\PembayaranTagihan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BulanTagihan extends Model
{
    use HasFactory;
    protected $table = 'bulan_tagihan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'bulan',
        'tahun',
    ];
    public function pembayaranTagihan(): HasMany
    {
        return $this->hasMany(PembayaranTagihan::class);
    }
}
