<?php

namespace App\Models;

use App\Models\Gangguan;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PivotGangguan extends Model
{
    use HasFactory;
    protected $table = 'pivot_gangguan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_gangguan',
        'id_teknisi',
    ];
    public function gangguan()
    {
        return $this->belongsTo(Gangguan::class, 'id_gangguan');
    }
    public function teknisi()
    {
        return $this->belongsTo(User::class, 'id_teknisi');
    }
}
