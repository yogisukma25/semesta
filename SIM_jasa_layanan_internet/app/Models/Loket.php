<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Loket extends Model
{
    use HasFactory;
    protected $table = 'loket';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'alamat',
    ];

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }
}
