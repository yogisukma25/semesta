<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Gangguan;
use App\Models\JenisIdentitas;
use App\Models\Loket;
use App\Models\PaketBerlangganan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles, HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'id_pelanggan',
        'tanggal_pemasangan',
        'id_jenis_identitas',
        'nomor_identitas',
        'tempat_lahir',
        'tanggal_lahir',
        'alamat',
        'latitude',
        'longitude',
        'nomor_telepon',
        'id_bts',
        'id_paket_berlangganan',
        'id_loket',
        'catatan',
        'status',
        'poin',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];
    public function bts(): BelongsTo
    {
        return $this->belongsTo(BTS::class);
    }
    public function jenisIdentitas(): BelongsTo
    {
        return $this->belongsTo(JenisIdentitas::class);
    }
    public function PaketBerlangganan(): BelongsTo
    {
        return $this->belongsTo(PaketBerlangganan::class);
    }
    public function Loket(): BelongsTo
    {
        return $this->belongsTo(Loket::class);
    }
    public function teknisiBantu(): BelongsToMany
    {
        return $this->belongsToMany(Gangguan::class, 'pivot_gangguan', 'id_gangguan', 'id_teknisi');
    }
}
