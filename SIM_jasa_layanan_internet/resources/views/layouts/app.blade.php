<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Datatables -->
    <script src="https://cdn.tailwindcss.com"></script>
    {{-- @vite(['resources/css/app.css', 'resources/js/app.js']) --}}
    <script>
      tailwind.config = {
        darkMode: 'class',
        fontFamily: {
          'body': [
        'Inter', 
        'ui-sans-serif', 
        'system-ui', 
        '-apple-system', 
        'system-ui', 
        'Segoe UI', 
        'Roboto', 
        'Helvetica Neue', 
        'Arial', 
        'Noto Sans', 
        'sans-serif', 
        'Apple Color Emoji', 
        'Segoe UI Emoji', 
        'Segoe UI Symbol', 
        'Noto Color Emoji'
      ],
          'sans': [
        'Inter', 
        'ui-sans-serif', 
        'system-ui', 
        '-apple-system', 
        'system-ui', 
        'Segoe UI', 
        'Roboto', 
        'Helvetica Neue', 
        'Arial', 
        'Noto Sans', 
        'sans-serif', 
        'Apple Color Emoji', 
        'Segoe UI Emoji', 
        'Segoe UI Symbol', 
        'Noto Color Emoji'
      ]
        }
      }
    </script>
    <!-- Sweet Alert -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css">
    <script>
        // On page load or when changing themes, best to add inline in `head` to avoid FOUC
        if (localStorage.getItem('color-theme') === 'dark' || (!('color-theme' in localStorage) && window.matchMedia(
                '(prefers-color-scheme: dark)').matches)) {
            document.documentElement.classList.add('dark');
        } else {
            document.documentElement.classList.remove('dark')
        }
    </script>

    <title>Semesta - Koneksi Telnet</title>
    <style>
        :root {
            --hue: 223;
            --bg: hsl(var(--hue), 90%, 95%);
            --fg: hsl(var(--hue), 90%, 5%);
            --trans-dur: 0.3s;
        }

        .page-loading {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
            -webkit-transition: all 0.4s 0.2s ease-in-out;
            transition: all 0.4s 0.2s ease-in-out;
            opacity: 0;
            visibility: hidden;
            z-index: 9999;
        }

        .page-loading.active {
            opacity: 1;
            visibility: visible;
        }

        .page-loading-inner {
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            text-align: center;
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            -webkit-transition: opacity 0.2s ease-in-out;
            transition: opacity 0.2s ease-in-out;
            opacity: 0;
        }

        .page-loading.active>.page-loading-inner {
            opacity: 1;
        }

        .page-loading-inner>span {
            display: block;
            font-size: 1rem;
            font-weight: normal;
            color: #9397ad;
        }

        .dark-mode .page-loading-inner>span {
            color: #fff;
            opacity: 0.6;
        }

        .dark-mode .page-spinner {
            border-color: rgba(255, 255, 255, 0.4);
            border-right-color: transparent;
        }

        .ip {
            display: inline-block;
            width: 7rem;
            height: 7rem;
            margin-bottom: 0.75rem;
            vertical-align: text-bottom;
        }

        .ip__track {
            stroke: hsl(var(--hue), 90%, 90%);
            transition: stroke var(--trans-dur);
        }

        .ip__worm1,
        .ip__worm2 {
            animation: worm1 2s linear infinite;
        }

        .ip__worm2 {
            animation-name: worm2;
        }

        /* Dark theme */
        @media (prefers-color-scheme: dark) {
            :root {
                --bg: hsl(var(--hue), 90%, 5%);
                --fg: hsl(var(--hue), 90%, 95%);
            }

            .ip__track {
                stroke: hsl(var(--hue), 90%, 15%);
            }
        }

        /* Animation */
        @keyframes worm1 {
            from {
                stroke-dashoffset: 0;
            }

            50% {
                animation-timing-function: steps(1);
                stroke-dashoffset: -358;
            }

            50.01% {
                animation-timing-function: linear;
                stroke-dashoffset: 358;
            }

            to {
                stroke-dashoffset: 0;
            }
        }

        @keyframes worm2 {
            from {
                stroke-dashoffset: 358;
            }

            50% {
                stroke-dashoffset: 0;
            }

            to {
                stroke-dashoffset: -358;
            }
        }
    </style>
</head>

<body>
    <!-- Preloader Start -->
    <div class="bg-white page-loading active dark:bg-gray-900">
        <div class="page-loading-inner">
            {{-- <div class="page-spinner"></div> --}}
            <svg class="ip" viewBox="0 0 256 128" width="256px" height="128px" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <linearGradient id="grad1" x1="0" y1="0" x2="1" y2="0">
                        <stop offset="0%" stop-color="#5ebd3e" />
                        <stop offset="33%" stop-color="#ffb900" />
                        <stop offset="67%" stop-color="#f78200" />
                        <stop offset="100%" stop-color="#e23838" />
                    </linearGradient>
                    <linearGradient id="grad2" x1="1" y1="0" x2="0" y2="0">
                        <stop offset="0%" stop-color="#e23838" />
                        <stop offset="33%" stop-color="#973999" />
                        <stop offset="67%" stop-color="#009cdf" />
                        <stop offset="100%" stop-color="#5ebd3e" />
                    </linearGradient>
                </defs>
                <g fill="none" stroke-linecap="round" stroke-width="16">
                    <g class="ip__track" stroke="#ddd">
                        <path d="M8,64s0-56,60-56,60,112,120,112,60-56,60-56" />
                        <path d="M248,64s0-56-60-56-60,112-120,112S8,64,8,64" />
                    </g>
                    <g stroke-dasharray="180 656">
                        <path class="ip__worm1" stroke="url(#grad1)" stroke-dashoffset="0"
                            d="M8,64s0-56,60-56,60,112,120,112,60-56,60-56" />
                        <path class="ip__worm2" stroke="url(#grad2)" stroke-dashoffset="358"
                            d="M248,64s0-56-60-56-60,112-120,112S8,64,8,64" />
                    </g>
                </g>
            </svg>
            <span>Loading...</span>
        </div>
    </div>
    <!-- Preloader Start -->

    @include('partials.navbar')
    @include('partials.sidebar')
    <main class="h-auto pt-10 md:ml-64">
        @yield('content')
    </main>
    </div>

    <script src="{{ asset('flowbite/dist/flowbite.min.js') }}"></script>
    <!-- Page loading scripts -->
    <script>
    (function() {
        // Menampilkan spinner ketika halaman dimuat
        const preloader = document.querySelector(".page-loading");
        preloader.classList.add("active");

        // Memeriksa URL saat ini
        function checkCurrentURL() {
            // Gantilah 'URL_tujuan_anda' dengan URL yang sesuai
            const targetURL = 'URL_tujuan_anda';

            if (window.location.href === targetURL) {
                // Jika URL saat ini adalah URL tujuan, maka hilangkan preloader
                preloader.classList.remove("active");
            }
        }

        // Memuat halaman baru berdasarkan tautan yang diklik
        function loadPage(url) {
            // Tambahkan logika yang sesuai di sini untuk mengarahkan pengguna ke URL yang diklik
            window.location.href = url;
        }

        // Memeriksa URL saat ini saat halaman dimuat
        checkCurrentURL();

        // Menghapus preloader setelah 3 detik
        setTimeout(function() {
            preloader.classList.remove("active");
        }, 3000);

        // Menambahkan event listener untuk tautan yang diklik
        const links = document.querySelectorAll("a"); // Anda juga dapat menyesuaikan selektor ini
        links.forEach(link => {
            link.addEventListener("click", function(event) {
                event.preventDefault(); // Mencegah tautan default dari membuka URL
                const clickedURL = link.getAttribute("href"); // Mengambil URL yang diklik
                loadPage(clickedURL); // Memuat halaman berdasarkan URL yang diklik
            });
        });
    })();
</script>

    <!-- Sweet Alert CSS and JS -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.all.min.js"></script>
    <script>
        function notificationBeforeLogout(event, el) {
            event.preventDefault();
            Swal.fire({
                title: 'Yakin ingin keluar?',
                text: 'Anda harus login kembali setelah ini!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#4e76e2',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Keluar!',
                cancelButtonText: 'Batal',
            }).then((result) => {
            if (result.isConfirmed) {
                // Submit the logout form
                document.getElementById('logout').submit();
            }
        });
        }
    </script>
    <script>
        var themeToggleDarkIcon = document.getElementById('theme-toggle-dark-icon');
        var themeToggleLightIcon = document.getElementById('theme-toggle-light-icon');

        // Change the icons inside the button based on previous settings
        if (localStorage.getItem('color-theme') === 'dark' || (!('color-theme' in localStorage) && window.matchMedia(
                '(prefers-color-scheme: dark)').matches)) {
            themeToggleLightIcon.classList.remove('hidden');
        } else {
            themeToggleDarkIcon.classList.remove('hidden');
        }

        var themeToggleBtn = document.getElementById('theme-toggle');

        themeToggleBtn.addEventListener('click', function() {

            // toggle icons inside button
            themeToggleDarkIcon.classList.toggle('hidden');
            themeToggleLightIcon.classList.toggle('hidden');

            // if set via local storage previously
            if (localStorage.getItem('color-theme')) {
                if (localStorage.getItem('color-theme') === 'light') {
                    document.documentElement.classList.add('dark');
                    localStorage.setItem('color-theme', 'dark');
                } else {
                    document.documentElement.classList.remove('dark');
                    localStorage.setItem('color-theme', 'light');
                }

                // if NOT set via local storage previously
            } else {
                if (document.documentElement.classList.contains('dark')) {
                    document.documentElement.classList.remove('dark');
                    localStorage.setItem('color-theme', 'light');
                } else {
                    document.documentElement.classList.add('dark');
                    localStorage.setItem('color-theme', 'dark');
                }
            }

        });
    </script>
    
    <!-- Datatables -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.tailwindcss.min.js"></script>
    
    <script>
        $(document).ready(function() {
            $('#data-table').DataTable();
        });
    </script>
</body>

</html>
