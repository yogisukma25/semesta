<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $superadmin = User::factory()->create([
        //     'name' => 'Super Admin',
        //     'email' => 'SuperAdmin@gmail.com',
        //     'password' => bcrypt('1234578'),
        // ]);
        // $superadmin->assignRole('superadmin');
        $admin = User::factory()->create([
            'name' => 'Admin',
            'email' => 'Admin@gmail.com',
            'password' => bcrypt('1234578'),
        ]);
        $admin->assignRole('admin');
        $teknisi = User::factory()->create([
            'name' => 'Teknisi',
            'email' => 'Teknisi@gmail.com',
            'password' => bcrypt('1234578'),
        ]);
        $teknisi->assignRole('teknisi');
        $pelanggan = User::factory()->create([
            'name' => 'Pelanggan',
            'email' => 'Pelanggan@gmail.com',
            'password' => bcrypt('1234578'),
        ]);
        $pelanggan->assignRole('pelanggan');
    }
}
