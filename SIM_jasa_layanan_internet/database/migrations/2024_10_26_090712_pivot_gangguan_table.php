<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pivot_gangguan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_gangguan');
            $table->unsignedBigInteger('id_teknisi');
            $table->timestamps();
            $table->foreign('id_gangguan')->references('id')->on('gangguan');
            $table->foreign('id_teknisi')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pivot_gangguan');
    }
};
