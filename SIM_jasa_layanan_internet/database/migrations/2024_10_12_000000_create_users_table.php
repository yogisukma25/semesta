<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->unsignedBigInteger('id_pelanggan')->nullable();
            $table->date('tanggal_pemasangan')->nullable();
            $table->unsignedBigInteger('id_jenis_identitas')->nullable();
            $table->string('nomor_identitas')->unique()->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('alamat')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('nomor_telepon')->nullable();
            $table->unsignedBigInteger('id_bts')->nullable();
            $table->unsignedBigInteger('id_paket_berlangganan')->nullable();
            $table->unsignedBigInteger('id_loket')->nullable();
            $table->string('catatan')->nullable();
            $table->enum('status', ['aktif', 'non-aktif'])->defaultValue('aktif');
            $table->integer('poin')->nullable();
            $table->timestamps();
            $table->foreign('id_jenis_identitas')->references('id')->on('jenis_identitas');
            $table->foreign('id_bts')->references('id')->on('bts');
            $table->foreign('id_paket_berlangganan')->references('id')->on('paket_berlangganan');
            $table->foreign('id_loket')->references('id')->on('loket');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
