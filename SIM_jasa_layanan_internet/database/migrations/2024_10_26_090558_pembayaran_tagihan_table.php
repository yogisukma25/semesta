<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pembayaran_tagihan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_bulan_tagihan');
            $table->unsignedBigInteger('id_pelanggan');
            $table->unsignedBigInteger('id_loket');
            $table->decimal('jumlah_bayar')->defaultValue('0');
            $table->date('tanggal_bayar')->nullable();
            $table->string('catatan')->nullable();
            $table->enum('status', ['paid', 'unpaid'])->defaultValue('unpaid');
            $table->timestamps();
            $table->foreign('id_bulan_tagihan')->references('id')->on('bulan_tagihan');
            $table->foreign('id_pelanggan')->references('id')->on('users');
            $table->foreign('id_loket')->references('id')->on('loket');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pembayaran_tagihan');
    }
};
