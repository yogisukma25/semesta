<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gangguan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_pelanggan');
            $table->unsignedBigInteger('id_teknisi')->nullable();
            $table->string('deskripsi');
            $table->string('bukti')->nullable();
            $table->string('catatan')->nullable();
            $table->enum('status', ['diproses', 'ditangani', 'selesai'])->defaultValue('diproses');
            $table->timestamps();
            $table->foreign('id_pelanggan')->references('id')->on('users');
            $table->foreign('id_teknisi')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gangguan');
    }
};
